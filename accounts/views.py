from django.shortcuts import render, redirect
from django.views import View
from .forms import RegisterUserForm, LoginUserForm
from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from .manager import MarvelManager


class LoginRegisterView(View):
    def get(self, request):
        asdfhasoidf
        template_name = 'index.html'
        ctx = {
            'loginForm':  LoginUserForm(),
            'registerForm': RegisterUserForm(),
        }
        return render(request, template_name, ctx)

    def post(self, request):
        if 'registro' in request.POST:
            print(request.POST)
            form = RegisterUserForm(request.POST)
            if form.is_valid():
                cd = form.cleaned_data
                User.objects.create_user(
                    username=cd['username'],
                    first_name=cd['first_name'],
                    password=cd['password']
                )
                messages.success(
                    request,
                    'Usuario creado'
                )
                return redirect('/mis-comics')
            else:
                template_name = 'index.html'
                ctx = {
                    'loginForm':  LoginUserForm(),
                    'registerForm': form,
                }
                return render(request, template_name, ctx)
        elif 'login' in request.POST:
            user = authenticate(
                username=request.POST['username'],
                password=request.POST['password']
            )
            if user is not None:
                login(request, user)
                return redirect('/mis-comics')
            else:
                messages.error(
                    request,
                    'Usuario o contraseña incorrectos'
                )
                return redirect('/')
        return redirect('/')



class ComicsUser(View):
    def get(self, request):
        template_name = 'comics.html'
        ctx = {
            'user':  request.user,
        }
        return render(request, template_name, ctx)
    
    def post(self, request):
        template_name = 'comics.html'
        ctx = {
            'user':  request.user,
        }
        if 'search' in request.POST:
            manager = MarvelManager()
            results = manager.get_comics(request.POST['search'])
            ctx['results'] = results
            return render(request, template_name, ctx)