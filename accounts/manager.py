import requests
import hashlib
import datetime

class MarvelManager(object):
    def __init__(self, *args, **kwargs):
        self.key = '256879b67ce1e867d0ae8bb1efe40c52'
        self.private_key = '5921ae498fe9cc11c1bd1e26800ed7170a09226b'
        self.url = 'https://gateway.marvel.com:443'

    def get_comics(self, title):
        ts = datetime.datetime.now()
        md5_hash = hashlib.md5()
        md5_hash.update(
            "{}{}{}".format(
                ts,
                self.private_key,
                self.key
            ).encode()
        )
        params = {
            'title': title,
            'hash': md5_hash.hexdigest(),
            'apikey': self.key,
            'ts': ts
        }
        response = requests.get(
            self.url + '/v1/public/comics',
            params
        )
        if response.status_code == 200:
            return response.json()['data']['results']
        else:
            return {}

    def get_comic(self, id):
        response = requests.get(
            self.url + '/v1/public/comics/{}?apikey={}'.format(id, self.key)
        )
        if response.status_code == 200:
            return response['data']['results']
        else:
            return {}