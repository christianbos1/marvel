from django.db import models
from django.contrib.auth.models import User


class ComicUser(models.Model):
    user = models.ForeignKey(User, related_name='comics', on_delete=models.CASCADE, blank=False, null=False)
    comic_id = models.IntegerField(blank=False, null=False)
    title = models.CharField(max_length=200,  blank=False, null=False)
    description = models.CharField(max_length=200,  blank=False, null=False)

    class Meta:
        verbose_name = 'Comic por usuario'

