from django.contrib import admin
from django.urls import path
from accounts.views import *


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', LoginRegisterView.as_view()),
    path('mis-comics', ComicsUser.as_view())
]
